@extends('layouts.app')

@section('head')
@endsection

@section('content')
    <div class="container">
        @component('components.login-info')
            @slot('title', "This is Home Page")
            @slot('type', "success")
        @endcomponent
    </div>
@endsection
