{{ $slot }}

<div class="card">
    <div class="card-header bg-{{ $type OR "" }}">{{ $title }}</div>
    <div class="card-body">
        @auth('web')
            <p class='text-success'>You are logged IN as <strong>USER</strong> ({{ Auth::guard('web')->user()->name }})</p>
        @else
            <p class="text-danger">You are logged OUT as <strong>USER</strong></p>
        @endauth
        
        @auth('admin')
            <p class="text-success">You are logged IN as <strong>ADMIN</strong> ({{ Auth::guard('admin')->user()->name }})</p>
        @else
            <p class="text-danger">You are logged OUT as <strong>ADMIN</strong></p>
        @endauth
    </div>
</div>


