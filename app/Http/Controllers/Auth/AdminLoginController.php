<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AdminLoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    public function login(Request $request) 
    {
        // validate request
        $this->validateLogin($request);
        

        // attempt to login
        if ($this->attemptLogin($request)) {
            // redirect page
            return redirect(route('admin.dashboard'));
        }

        return redirect(route('admin.login'));
    }

    public function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email'=>'required|email',
            'password'=>'required|min:6'
        ]);
    }

    public function attemptLogin(Request $request)
    {
        return Auth::guard('admin')->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    public function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    protected function authenticated(Request $request, $user)
    {
        //
    }

}
