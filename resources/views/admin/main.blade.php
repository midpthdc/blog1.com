@extends('admin.layouts.app')

@section('head')
@endsection

@section('content')
    <div class="container">
        @component('components.login-info')
            @slot('title', "This is Admin Page")
            @slot('type', "info")
        @endcomponent
    </div>
@endsection